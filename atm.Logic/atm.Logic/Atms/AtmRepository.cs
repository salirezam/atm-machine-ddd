﻿using atm.Logic.Common;
using atm.Logic.Utils;
using System.Collections.Generic;
using System.Linq;

namespace atm.Logic.Atms
{
    public class AtmRepository: Repository<Atm>
    {
        public IReadOnlyList<AtmDto> GetAtmList()
        {
            using (var session = SessionFactory.OpenSession())
            {
                return session.Query<Atm>()
                    .ToList() // Fetch data into memory
                    .Select(x => new AtmDto(x.Id, x.MoneyInside.Amount))
                    .ToList();
            }
        }
    }
}
