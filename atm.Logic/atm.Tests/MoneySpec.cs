﻿using atm.Logic.SharedKernel;
using FluentAssertions;
using System;
using Xunit;

namespace atm.Tests
{
    public class MoneySpec
    {
        [Fact]
        public void Sum_of_two_moneys_produces_correct_result()
        {
            var money1 = new Money(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            var money2 = new Money(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);

            var sum = money1 + money2;

            sum.OnePennyCount.Should().Be(2);
            sum.TwoPennyCount.Should().Be(4);
            sum.FivePennyCount.Should().Be(6);
            sum.TenPennyCount.Should().Be(8);
            sum.TwentyPennyCount.Should().Be(10);
            sum.FiftyPennyCount.Should().Be(12);
            sum.OnePoundCount.Should().Be(14);
            sum.FivePoundCount.Should().Be(16);
            sum.TenPoundCount.Should().Be(18);
            sum.TwentyPoundCount.Should().Be(20);
            sum.FiftyPoundCount.Should().Be(22);
        }

        [Fact]
        public void Two_money_instances_equal_if_contain_the_same_money_amount()
        {
            var money1 = new Money(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            var money2 = new Money(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);

            money1.Should().Be(money2);
            money1.GetHashCode().Should().Be(money2.GetHashCode());
        }

        [Fact]
        public void Two_money_instances_do_not_equal_if_contain_different_money_amounts()
        {
            var pound = new Money(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);
            var hundredPenny = new Money(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            pound.Should().NotBe(hundredPenny);
            pound.GetHashCode().Should().NotBe(hundredPenny.GetHashCode());
        }

        [Fact]
        public void Subtraction_of_two_moneys_produces_correct_result()
        {
            var money1 = new Money(11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11);
            var money2 = new Money(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);

            var result = money1 - money2;

            result.OnePennyCount.Should().Be(10);
            result.TwoPennyCount.Should().Be(9);
            result.FivePennyCount.Should().Be(8);
            result.TenPennyCount.Should().Be(7);
            result.TwentyPennyCount.Should().Be(6);
            result.FiftyPennyCount.Should().Be(5);
            result.OnePoundCount.Should().Be(4);
            result.FivePoundCount.Should().Be(3);
            result.TenPoundCount.Should().Be(2);
            result.TwentyPoundCount.Should().Be(1);
            result.FiftyPoundCount.Should().Be(0);
        }

        [Fact]
        public void Cannot_subtract_more_than_exists()
        {
            var money1 = new Money(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);
            var money2 = new Money(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            Action action = () =>
            {
                var money = money1 - money2;
            };

            action.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData( -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [InlineData( 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [InlineData( 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0)]
        [InlineData( 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0)]
        [InlineData( 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0)]
        [InlineData( 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0)]
        [InlineData( 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0)]
        [InlineData( 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0)]
        [InlineData( 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0)]
        [InlineData( 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0)]
        [InlineData( 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1)]
        public void Cannot_create_money_with_negative_value(int onePennyCount, int twoPennyCount, int fivePennyCount, int tenPennyCount, int twentyPennyCount, int fiftyPennyCount, int onePoundCount, int fivePoundCount, int tenPoundCount, int twentyPoundCount, int fiftyPoundCount)
        {
            Action action = () => new Money(
                onePennyCount,
                twoPennyCount,
                fivePennyCount,
                tenPennyCount,
                twentyPennyCount,
                fiftyPennyCount,
                onePoundCount,
                fivePoundCount,
                tenPoundCount,
                twentyPoundCount,
                fiftyPoundCount);

            action.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)]
        [InlineData(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.01)]
        [InlineData(1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.05)]
        [InlineData(1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0.20)]
        [InlineData(1, 2, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0.60)]
        [InlineData(1, 2, 3, 4, 5, 0, 0, 0, 0, 0, 0, 1.60)]
        [InlineData(1, 2, 3, 4, 5, 6, 0, 0, 0, 0, 0, 4.60)]
        [InlineData(1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0, 11.60)]
        [InlineData(1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 51.60)]
        [InlineData(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 0, 141.60)]
        [InlineData(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 341.60)]
        [InlineData(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 891.60)]
        public void Amount_is_calculated_correctly(int onePennyCount, int twoPennyCount, int fivePennyCount, int tenPennyCount, int twentyPennyCount, int fiftyPennyCount, int onePoundCount, int fivePoundCount, int tenPoundCount, int twentyPoundCount, int fiftyPoundCount, decimal expectedAmount)
        {
            var money = new Money(
                onePennyCount,
                twoPennyCount,
                fivePennyCount,
                tenPennyCount,
                twentyPennyCount,
                fiftyPennyCount,
                onePoundCount,
                fivePoundCount,
                tenPoundCount,
                twentyPoundCount,
                fiftyPoundCount);

            money.Amount.Should().Be(expectedAmount);
        }

    }
}
