﻿using atm.Logic.Utils;
using System.Windows;

namespace atm.UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Initer.Init(@"Server=.\SQLEXPRESS;Database=atmDb;Trusted_Connection=true");
        }
    }
}
