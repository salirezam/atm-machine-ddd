﻿using atm.Logic.Common;
using atm.Logic.SharedKernel;
using System;
using static atm.Logic.SharedKernel.Money;

namespace atm.Logic.Atms
{
    public class Atm: AggregateRoot
    {
        private const decimal CommisionRate = 0.01m;

        public virtual Money MoneyInside { get; protected set; } = None;
        public virtual decimal MoneyCharged { get; protected set; }

        public virtual string CanTakeMoney(decimal amount)
        {
            if (amount <= 0m)
                return "Invalid amount";

            if (MoneyInside.Amount < amount)
                return "Not enough change";

            if (!MoneyInside.CanAllocate(amount))
                return "Not enough change";

            return string.Empty;
        }

        public virtual void TakeMoney(decimal amount)
        {
            if (CanTakeMoney(amount) != string.Empty)
                throw new InvalidOperationException();

            var output = MoneyInside.Allocate(amount);
            MoneyInside -= output;

            var amountWithCommission = CalculateAmountWithCommission(amount);
            MoneyCharged += amountWithCommission;

            AddDomainEvent(new BalanceChangedEvent(amountWithCommission));
        }

        public virtual decimal CalculateAmountWithCommission(decimal amount)
        {
            var commission = amount * CommisionRate;
            var lessThanPenny = commission % 0.01m;
            if(lessThanPenny > 0)
            {
                commission = commission - lessThanPenny + 0.01m;
            }

            return amount + commission;
        }

        public virtual void LoadMoney(Money money)
        {
            MoneyInside += money;
        }
    }
}
