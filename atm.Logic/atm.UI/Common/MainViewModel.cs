﻿using atm.Logic.Atms;
using atm.Logic.SnackMachines;
using atm.UI.Atms;
using atm.UI.Management;
using atm.UI.SnackMachines;

namespace atm.UI.Common
{
    public class MainViewModel : ViewModel
    {
        public DashboardViewModel Dashboard { get; private set; }
        public MainViewModel()
        {
            //var repository = new SnackMachineRepository();
            //var snackMachine = repository.GetById(1L);

            //var viewModel = new SnackMachineViewModel(snackMachine);
            //_dialogService.ShowDialog(viewModel);
            Dashboard = new DashboardViewModel();
            _dialogService.ShowDialog(Dashboard);
        }
    }
}
