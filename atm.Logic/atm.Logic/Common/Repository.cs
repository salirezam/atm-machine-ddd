﻿
using atm.Logic.Utils;
using NHibernate;

namespace atm.Logic.Common
{
    public abstract class Repository<T> where T : AggregateRoot
    {
        public T GetById(long Id)
        {
            using(var session = SessionFactory.OpenSession())
            {
                return session.Get<T>(Id);
            }
        }

        public void Save(T aggregateRoot)
        {
            using (var session = SessionFactory.OpenSession())
                using(var transaction = session.BeginTransaction())
            {
                session.SaveOrUpdate(aggregateRoot);
                transaction.Commit();
            }
        }
    }
}
