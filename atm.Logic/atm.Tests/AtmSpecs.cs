﻿using atm.Logic.Atms;
using static atm.Logic.SharedKernel.Money;
using Xunit;
using FluentAssertions;
using System.Linq;

namespace atm.Tests
{
    public class AtmSpecs
    {
        [Fact]
        public void Take_money_exchnages_with_commissions()
        {
            var atm = new Atm();
            atm.LoadMoney(Pound);

            atm.TakeMoney(1m);

            atm.MoneyInside.Amount.Should().Be(0);
            atm.MoneyCharged.Should().Be(1.01m);
        }

        [Fact]
        public void Commission_is_at_least_one_penny()
        {
            var atm = new Atm();
            atm.LoadMoney(Penny);

            atm.TakeMoney(0.01m);

            atm.MoneyCharged.Should().Be(0.02m);
        }

        [Fact]
        public void Commission_is_rounded_up_to_the_next_penny()
        {
            var atm = new Atm();
            atm.LoadMoney(Pound + TenPenny);

            atm.TakeMoney(1.1m);

            atm.MoneyCharged.Should().Be(1.12m);
        }


        [Fact]
        public void Take_money_raises_an_event()
        {
            var atm = new Atm();
            atm.LoadMoney(Pound);

            atm.TakeMoney(1m);

            atm.ShouldContainBalanceChangedEvent(1.01m);
        }
    }

    internal static class AtmExtensions
    {
        public static void ShouldContainBalanceChangedEvent(this Atm atm, decimal delta)
        {
            var domainEvent = (BalanceChangedEvent)atm.DomainEvents
                .SingleOrDefault(x => x.GetType() == typeof(BalanceChangedEvent));

            domainEvent.Should().NotBeNull();
            domainEvent.Delta.Should().Be(delta);
        }
    }
}
