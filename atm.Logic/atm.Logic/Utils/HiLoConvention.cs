﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace atm.Logic.Utils
{
    public class HiLoConvention : IIdConvention
    {
        public void Apply(IIdentityInstance instance)
        {
            instance.Column(instance.EntityType.Name + "ID");
            instance.GeneratedBy.HiLo("[dbo].[Ids]","NextHigh", "9", $"EntityName = {instance.EntityType.Name}");
        }
    }
}