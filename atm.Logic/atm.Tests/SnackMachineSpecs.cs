﻿using static atm.Logic.SharedKernel.Money;
using System;
using Xunit;
using FluentAssertions;
using atm.Logic.SnackMachines;

namespace atm.Tests
{
    public class SnackMachineSpecs
    {
        [Fact]
        public void Return_money_empties_money_in_transaction()
        {
            var snackMachine = new SnackMachine();
            snackMachine.InsertMoney(Pound);

            snackMachine.ReturnMoney();

            snackMachine.MoneyInTransaction.Should().Be(0);
        }

        [Fact]
        public void Inserted_money_goes_to_money_in_transaction()
        {
            var snackMachine = new SnackMachine();
            snackMachine.InsertMoney(Pound);
            snackMachine.InsertMoney(Penny);

            snackMachine.MoneyInTransaction.Should().Be(1.01m);
        }

        [Fact]
        public void Cannot_insert_more_than_one_coin_or_note_at_a_time()
        {
            var snackMachine = new SnackMachine();
            var threePenny = Penny + TwentyPenny;

            Action action = () => snackMachine.InsertMoney(threePenny);

            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void BuySnack_trades_inserted_money_for_a_snack()
        {
            var snackMachine = new SnackMachine();
            snackMachine.LoadSnacks(1,new SnackPile(Snack.Chocolate, 10, 1m));
            snackMachine.InsertMoney(Pound);

            snackMachine.BuySnack(1);

            snackMachine.MoneyInTransaction.Should().Be(0);
            snackMachine.MoneyInside.Amount.Should().Be(1m);
            snackMachine.GetSnackPile(1).Quantity.Should().Be(9);
        }

        [Fact]
        public void Cannot_make_purchase_when_there_is_no_snacks()
        {
            var snackMachine = new SnackMachine();

            Action action = () => snackMachine.BuySnack(1);

            action.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData(-1)]
        public void Cannot_add_a_snackpile_with_negative_quantity(int quantity)
        {
            var snackMachine = new SnackMachine();
            Action action = () => snackMachine.LoadSnacks(1, new SnackPile(Snack.Chocolate, quantity, 1m));
            action.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData(-1)]
        public void Cannot_add_a_snackpile_with_negative_price(decimal price)
        {
            var snackMachine = new SnackMachine();
            Action action = () => snackMachine.LoadSnacks(1, new SnackPile(Snack.Chocolate, 1, price));
            action.Should().Throw<InvalidOperationException>();
        }

        [Theory]
        [InlineData(0.001)]
        public void Cannot_add_a_snackpile_with_a_value_less_than_a_penny(decimal price)
        {
            var snackMachine = new SnackMachine();
            Action action = () => snackMachine.LoadSnacks(1, new SnackPile(Snack.Chocolate, 1, price));
            action.Should().Throw<InvalidOperationException>();
        }

        [Fact]
        public void Snack_machine_returns_money_with_highest_denomination_first()
        {
            var snackMachine = new SnackMachine();
            snackMachine.LoadMoney(Pound);

            snackMachine.InsertMoney(TwentyPenny);
            snackMachine.InsertMoney(TwentyPenny);
            snackMachine.InsertMoney(TwentyPenny);
            snackMachine.InsertMoney(TwentyPenny);
            snackMachine.InsertMoney(TwentyPenny);
            snackMachine.ReturnMoney();

            snackMachine.MoneyInside.TwentyPennyCount.Should().Be(5);
            snackMachine.MoneyInside.OnePoundCount.Should().Be(0);
        }

        [Fact]
        public void After_purchase_change_is_returned()
        {
            var snackMachine = new SnackMachine();
            snackMachine.LoadSnacks(1, new SnackPile(Snack.Chocolate, 1, 0.5m));
            snackMachine.LoadMoney(TenPenny * 10);

            snackMachine.InsertMoney(Pound);
            snackMachine.BuySnack(1);

            snackMachine.MoneyInside.Amount.Should().Be(1.5m);
            snackMachine.MoneyInTransaction.Should().Be(0m);
        }

        [Fact]
        public void Cannot_buy_snack_if_not_enough_change()
        {
            var snackMachine = new SnackMachine();
            snackMachine.LoadSnacks(1, new SnackPile(Snack.Chocolate, 1, 0.5m));
            snackMachine.InsertMoney(Pound);

            Action action = () =>snackMachine.BuySnack(1);

            action.Should().Throw<InvalidOperationException>();
        }
    }
}
