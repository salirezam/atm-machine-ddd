﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using atm.Logic.Common;

namespace atm.Logic.Atms
{
    public class BalanceChangedEvent : IDomainEvent
    {
        public decimal Delta { get; private set; }
        public BalanceChangedEvent(decimal delta)
        {
            Delta = delta;
        }
    }
}
