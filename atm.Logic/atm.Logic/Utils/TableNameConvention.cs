﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.Instances;

namespace atm.Logic.Utils
{
    public class TableNameConvention : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            instance.Table($"[dbo].[{instance.EntityType.Name}]");
        }
    }
}