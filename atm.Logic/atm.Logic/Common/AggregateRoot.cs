﻿using System.Collections.Generic;

namespace atm.Logic.Common
{
    public class AggregateRoot: Entity
    {
        private readonly List<IDomainEvent> _domainEvents = new List<IDomainEvent>();
        public virtual IReadOnlyList<IDomainEvent> DomainEvents => _domainEvents;

        protected virtual void AddDomainEvent(IDomainEvent newDomainEvent)
        {
            _domainEvents.Add(newDomainEvent);
        }

        public virtual void ClearEvents()
        {
            _domainEvents.Clear();
        }
    }
}
