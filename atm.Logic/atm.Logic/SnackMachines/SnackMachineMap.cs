﻿using FluentNHibernate;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace atm.Logic.SnackMachines
{
    public class SnackMachineMap : ClassMap<SnackMachine>
    {
        public SnackMachineMap()
        {
            Id(x => x.Id);

            Component(x => x.MoneyInside, y => 
            {
                y.Map(x => x.OnePennyCount);
                y.Map(x => x.TwoPennyCount);
                y.Map(x => x.FivePennyCount);
                y.Map(x => x.TenPennyCount);
                y.Map(x => x.TwentyPennyCount);
                y.Map(x => x.FiftyPennyCount);
                y.Map(x => x.OnePoundCount);
                y.Map(x => x.FivePoundCount);
                y.Map(x => x.TenPoundCount);
                y.Map(x => x.TwentyPoundCount);
                y.Map(x => x.FiftyPoundCount);

            });

            HasMany<Slot>(Reveal.Member<SnackMachine>("Slots"))
                .Cascade.SaveUpdate()
                .Not.LazyLoad()
                .Inverse();

        }
    }
}
