﻿using FluentNHibernate.Mapping;

namespace atm.Logic.Management
{
    public class HeadOfficeMap : ClassMap<HeadOffice>
    {
        public HeadOfficeMap()
        {
            Id(x => x.Id);
            Map(x => x.Balance);
            Component(x => x.Cash, y => {
                y.Map(x => x.OnePennyCount);
                y.Map(x => x.TwoPennyCount);
                y.Map(x => x.FivePennyCount);
                y.Map(x => x.TenPennyCount);
                y.Map(x => x.TwentyPennyCount);
                y.Map(x => x.FiftyPennyCount);
                y.Map(x => x.OnePoundCount);
                y.Map(x => x.FivePoundCount);
                y.Map(x => x.TenPoundCount);
                y.Map(x => x.TwentyPoundCount);
                y.Map(x => x.FiftyPoundCount);
            });
        }
    }
}
