USE [master]
GO
CREATE DATABASE [atm]
GO
USE [atm]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ids](
	[EntityName] [nvarchar](100) NOT NULL,
	[NextHigh] [int] NOT NULL,
 CONSTRAINT [PK_Ids] PRIMARY KEY CLUSTERED 
(
	[EntityName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SnackMachine](
	[SnackMachineID] [bigint] IDENTITY(1,1) NOT NULL,
	[OnePennyCount] [int] NOT NULL,
	[TwoPennyCount] [int] NOT NULL,
	[FivePennyCount] [int] NOT NULL,
	[TenPennyCount] [int] NOT NULL,
	[TwentyPennyCount] [int] NOT NULL,
	[FiftyPennyCount] [int] NOT NULL,
	[OnePoundCount] [int] NOT NULL,
	[FivePoundCount] [int] NOT NULL,
	[TenPoundCount] [int] NOT NULL,
	[TwentyPoundCount] [int] NOT NULL,
	[FiftyPoundCount] [int] NOT NULL,
 CONSTRAINT [PK_SnackMachine] PRIMARY KEY CLUSTERED 
(
	[SnackMachineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [atmDb].[dbo].[Atm](
	[AtmID] [bigint] NOT NULL,
	[MoneyCharged] [decimal](18, 2) NOT NULL,
	[OnePennyCount] [int] NOT NULL,
	[TwoPennyCount] [int] NOT NULL,
	[FivePennyCount] [int] NOT NULL,
	[TenPennyCount] [int] NOT NULL,
	[TwentyPennyCount] [int] NOT NULL,
	[FiftyPennyCount] [int] NOT NULL,
	[OnePoundCount] [int] NOT NULL,
	[FivePoundCount] [int] NOT NULL,
	[TenPoundCount] [int] NOT NULL,
	[TwentyPoundCount] [int] NOT NULL,
	[FiftyPoundCount] [int] NOT NULL,
 CONSTRAINT [PK_Atm] PRIMARY KEY CLUSTERED 
(
	[AtmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [atmDb].[dbo].[HeadOffice](
	[HeadOfficeID] [bigint] NOT NULL,
	[Balance] [decimal](18, 2) NOT NULL,
	[OnePennyCount] [int] NOT NULL,
	[TwoPennyCount] [int] NOT NULL,
	[FivePennyCount] [int] NOT NULL,
	[TenPennyCount] [int] NOT NULL,
	[TwentyPennyCount] [int] NOT NULL,
	[FiftyPennyCount] [int] NOT NULL,
	[OnePoundCount] [int] NOT NULL,
	[FivePoundCount] [int] NOT NULL,
	[TenPoundCount] [int] NOT NULL,
	[TwentyPoundCount] [int] NOT NULL,
	[FiftyPoundCount] [int] NOT NULL,
 CONSTRAINT [PK_HeadOffice] PRIMARY KEY CLUSTERED 
(
	[HeadOfficeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

/****** Object:  Table [dbo].[Slot]   ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Slot](
	[SlotID] [bigint] NOT NULL,
	[Quantity] [int] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[SnackID] [bigint] NOT NULL,
	[SnackMachineID] [bigint] NOT NULL,
	[Position] [int] NOT NULL,
 CONSTRAINT [PK_Slot] PRIMARY KEY CLUSTERED 
(
	[SlotID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Snack]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Snack](
	[SnackID] [bigint] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Snack] PRIMARY KEY CLUSTERED 
(
	[SnackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/***********************/
GO

INSERT INTO [dbo].[Atm]
           ([AtmID]
           ,[MoneyCharged]
           ,[OnePennyCount]
           ,[TwoPennyCount]
           ,[FivePennyCount]
           ,[TenPennyCount]
           ,[TwentyPennyCount]
           ,[FiftyPennyCount]
           ,[OnePoundCount]
           ,[FivePoundCount]
           ,[TenPoundCount]
           ,[TwentyPoundCount]
           ,[FiftyPoundCount])
     VALUES
          (1, CAST(0 AS Decimal(18, 2)), 20, 20, 20, 20,20, 20, 20, 20, 20, 20,20, 20)
GO

INSERT INTO [dbo].[HeadOffice]
           ([HeadOfficeID]
           ,[Balance]
           ,[OnePennyCount]
           ,[TwoPennyCount]
           ,[FivePennyCount]
           ,[TenPennyCount]
           ,[TwentyPennyCount]
           ,[FiftyPennyCount]
           ,[OnePoundCount]
           ,[FivePoundCount]
           ,[TenPoundCount]
           ,[TwentyPoundCount]
           ,[FiftyPoundCount])
     VALUES
           (1, CAST(0 AS Decimal(18, 2)), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
GO
INSERT [dbo].[Ids] ([EntityName], [NextHigh]) VALUES (N'Atm', 1)
GO
INSERT [dbo].[Ids] ([EntityName], [NextHigh]) VALUES (N'HeadOffice', 1)
GO
INSERT [dbo].[Ids] ([EntityName], [NextHigh]) VALUES (N'Slot', 1)
GO
INSERT [dbo].[Ids] ([EntityName], [NextHigh]) VALUES (N'Snack', 1)
GO
INSERT [dbo].[Ids] ([EntityName], [NextHigh]) VALUES (N'SnackMachine', 1)
GO
INSERT [dbo].[Slot] ([SlotID], [Quantity], [Price], [SnackID], [SnackMachineID], [Position]) VALUES (1, 10, CAST(3.00 AS Decimal(18, 2)), 1, 1, 1)
GO
INSERT [dbo].[Slot] ([SlotID], [Quantity], [Price], [SnackID], [SnackMachineID], [Position]) VALUES (2, 15, CAST(2.00 AS Decimal(18, 2)), 2, 1, 2)
GO
INSERT [dbo].[Slot] ([SlotID], [Quantity], [Price], [SnackID], [SnackMachineID], [Position]) VALUES (3, 20, CAST(1.00 AS Decimal(18, 2)), 3, 1, 3)
GO
INSERT [dbo].[Snack] ([SnackID], [Name]) VALUES (1, N'Chocolate')
GO
INSERT [dbo].[Snack] ([SnackID], [Name]) VALUES (2, N'Soda')
GO
INSERT [dbo].[Snack] ([SnackID], [Name]) VALUES (3, N'Gum')
GO
SET IDENTITY_INSERT [dbo].[SnackMachine] ON 
GO

INSERT INTO [dbo].[SnackMachine]
           ([OnePennyCount]
           ,[TwoPennyCount]
           ,[FivePennyCount]
           ,[TenPennyCount]
           ,[TwentyPennyCount]
           ,[FiftyPennyCount]
           ,[OnePoundCount]
           ,[FivePoundCount]
           ,[TenPoundCount]
           ,[TwentyPoundCount]
           ,[FiftyPoundCount])
     VALUES
          (1, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10)
GO
SET IDENTITY_INSERT [dbo].[SnackMachine] OFF
GO
ALTER TABLE [dbo].[Slot] ADD  CONSTRAINT [DF_Slot_Position]  DEFAULT ((1)) FOR [Position]
GO
ALTER TABLE [dbo].[Slot]  WITH CHECK ADD  CONSTRAINT [FK_Slot_Snack] FOREIGN KEY([SnackID])
REFERENCES [dbo].[Snack] ([SnackID])
GO
ALTER TABLE [dbo].[Slot] CHECK CONSTRAINT [FK_Slot_Snack]
GO
ALTER TABLE [dbo].[Slot]  WITH CHECK ADD  CONSTRAINT [FK_Slot_SnackMachine] FOREIGN KEY([SnackMachineID])
REFERENCES [dbo].[SnackMachine] ([SnackMachineID])
GO
ALTER TABLE [dbo].[Slot] CHECK CONSTRAINT [FK_Slot_SnackMachine]
GO
USE [master]
GO
ALTER DATABASE [atm] SET  READ_WRITE 
GO
