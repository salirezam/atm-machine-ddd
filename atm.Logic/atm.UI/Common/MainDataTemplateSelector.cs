﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace atm.UI.Common
{
    public class MainDataTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item == null || Application.Current == null)
                return null;

            var name = item.GetType().Name;
            var template = Application.Current.TryFindResource(name) as DataTemplate;

            if(template == null)
            {
                throw new ArgumentException($"Template for ViewModel {name} was not found");
            }

            return template;
        }
    }
}
