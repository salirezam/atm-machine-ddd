﻿using atm.Logic.SharedKernel;
using atm.Logic.SnackMachines;
using atm.UI.Common;
using System.Collections.Generic;
using System.Linq;

namespace atm.UI.SnackMachines
{
    /// <summary>
    /// Interaction logic for SnackMachineViewModel.xaml
    /// </summary>
    public partial class SnackMachineViewModel : ViewModel
    {
        private readonly SnackMachine _snackMachine;
        private readonly SnackMachineRepository _repository;
        public override string Caption => "Snack Machine";

        public string MoneyInTransaction => _snackMachine.MoneyInTransaction.ToString();
        public Money MoneyInside => _snackMachine.MoneyInside;

        public IReadOnlyList<SnackPileViewModel> Piles => _snackMachine.GetAllSnackPiles()
                   .Select(x => new SnackPileViewModel(x))
                   .ToList();

        private string _message = "";
        public string Message
        {
            get { return _message; }
            private set
            {
                _message = value;
                Notify();
            }
        }

        public Command InsertPennyCommand { get; private set; }
        public Command InsertTwoPennyCommand { get; private set; }
        public Command InsertFivePennyCommand { get; private set; }
        public Command InsertTenPennyCommand { get; private set; }
        public Command InsertTwentyPennyCommand { get; private set; }
        public Command InsertFiftyPennyCommand { get; private set; }
        public Command InsertPoundCommand { get; private set; }
        public Command InsertFivePoundCommand { get; private set; }
        public Command InsertTenPoundCommand { get; private set; }
        public Command InsertTwentyPoundCommand { get; private set; }
        public Command InsertFiftyPoundCommand { get; private set; }
        public Command ReturnMoneyCommand { get; private set; }
        public Command<string> BuySnackCommand { get; private set; }


        public SnackMachineViewModel(SnackMachine snackMachine)
        {
            _snackMachine = snackMachine;
            _repository = new SnackMachineRepository();

            InsertPennyCommand = new Command(() => InsertMoney(Money.Penny));
            InsertTwoPennyCommand = new Command(() => InsertMoney(Money.TwoPenny));
            InsertFivePennyCommand = new Command(() => InsertMoney(Money.FivePenny));
            InsertTenPennyCommand = new Command(() => InsertMoney(Money.TenPenny));
            InsertTwentyPennyCommand = new Command(() => InsertMoney(Money.TwentyPenny));
            InsertFiftyPennyCommand = new Command(() => InsertMoney(Money.FiftyPenny));
            InsertPoundCommand = new Command(() => InsertMoney(Money.Pound));
            InsertFivePoundCommand = new Command(() => InsertMoney(Money.FivePound));
            InsertTenPoundCommand = new Command(() => InsertMoney(Money.TenPound));
            InsertTwentyPoundCommand = new Command(() => InsertMoney(Money.TwentyPound));
            InsertFiftyPoundCommand = new Command(() => InsertMoney(Money.FiftyPound));
            ReturnMoneyCommand = new Command(() => ReturnMoney());
            BuySnackCommand = new Command<string>(BuySnack);
        }

        private void BuySnack(string positionString)
        {
            var  position = int.Parse(positionString);

            var error = _snackMachine.CanBuySnack(position);
            if (error != string.Empty)
            {
                NotifyClient(error);
                return;
            }

            _snackMachine.BuySnack(position);
            _repository.Save(_snackMachine);
            NotifyClient("You have bought a snack");
        }

        private void ReturnMoney()
        {
            _snackMachine.ReturnMoney();
            NotifyClient("Money was returned");
        }

        private void InsertMoney(Money coinOrNote)
        {
            _snackMachine.InsertMoney(coinOrNote);
            NotifyClient("You have inserted: " + coinOrNote);
        }

        private void NotifyClient(string message)
        {
            Message = message;
            Notify(nameof(MoneyInTransaction));
            Notify(nameof(MoneyInside));
            Notify(nameof(Piles));
        }
    }
}
