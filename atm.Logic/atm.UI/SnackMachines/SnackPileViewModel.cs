﻿using atm.Logic.SnackMachines;

namespace atm.UI.SnackMachines
{
    public class SnackPileViewModel
    {
        private readonly SnackPile _snackPile;

        public string Price => _snackPile.Price.ToString("C2");
        public string Name => _snackPile.Snack.Name;
        public int Amount => _snackPile.Quantity;
        public SnackPileViewModel(SnackPile snackPile)
        {
            _snackPile = snackPile;
        }
    }
}
