﻿using atm.Logic.Common;
using System;

namespace atm.Logic.SharedKernel
{
    public class Money: ValueObject<Money>
    {
        public static readonly Money None        = new Money(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Money Penny       = new Money(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Money TwoPenny    = new Money(0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Money FivePenny   = new Money(0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Money TenPenny    = new Money(0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0);
        public static readonly Money TwentyPenny = new Money(0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0);
        public static readonly Money FiftyPenny  = new Money(0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0);
        public static readonly Money Pound       = new Money(0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0);
        public static readonly Money FivePound   = new Money(0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0);
        public static readonly Money TenPound    = new Money(0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0);
        public static readonly Money TwentyPound = new Money(0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0);
        public static readonly Money FiftyPound  = new Money(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

        public int OnePennyCount { get; private set; }
        public int TwoPennyCount { get; private set; }
        public int FivePennyCount { get; private set; }
        public int TenPennyCount { get; private set; }
        public int TwentyPennyCount { get; private set; }
        public int FiftyPennyCount { get; private set; }
        public int OnePoundCount { get; private set; }

        internal Money AllocateCore(decimal amount)
        {
            var fiftyPoundCount = Math.Min((int)(amount / 50), FiftyPoundCount);
            amount = amount - fiftyPoundCount * 50;

            var twentyPoundCount = Math.Min((int)(amount / 20), TwentyPoundCount);
            amount = amount - twentyPoundCount * 20;

            var tenPoundCount = Math.Min((int)(amount / 10), TenPoundCount);
            amount = amount - tenPoundCount * 10;

            var fivePoundCount = Math.Min((int)(amount / 5), FivePoundCount);
            amount = amount - fivePoundCount * 5;

            var onePoundCount = Math.Min((int)amount, OnePoundCount);
            amount = amount - onePoundCount;

            var fiftyPennyCount = Math.Min((int)(amount / 0.5m), FiftyPennyCount);
            amount = amount - fiftyPennyCount * 0.5m;

            var twentyPennyCount = Math.Min((int)(amount / 0.25m), TwentyPennyCount);
            amount = amount - twentyPennyCount * 0.25m;

            var tenPennyCount = Math.Min((int)(amount / 0.1m), TenPennyCount);
            amount = amount - tenPennyCount * 0.1m;

            var fivePennyCount = Math.Min((int)(amount / 0.05m), FivePennyCount);
            amount = amount - fiftyPennyCount * 0.05m;

            var twoPennyCount = Math.Min((int)(amount / 0.02m), TwoPennyCount);
            amount = amount - twoPennyCount * 0.02m;

            var onePennyCount = Math.Min((int)(amount / 0.01m), OnePennyCount);
            amount = amount - onePennyCount * 0.01m;

            return new Money(
                onePennyCount,
                twoPennyCount,
                fivePennyCount,
                tenPennyCount,
                twentyPennyCount,
                fiftyPennyCount,
                onePoundCount,
                fivePoundCount,
                tenPoundCount,
                twentyPoundCount,
                fiftyPoundCount);
        }

        public bool CanAllocate(decimal amount)
        {
            var money = AllocateCore(amount);
            return money.Amount == amount;
        }

        public Money Allocate(decimal amount)
        {
            if (!CanAllocate(amount))
                throw new InvalidOperationException();

            return AllocateCore(amount);
        }

        public int FivePoundCount { get; private set; }
        public int TenPoundCount { get; private set; }
        public int TwentyPoundCount { get; private set; }
        public int FiftyPoundCount { get; private set; }

        public decimal Amount =>
            OnePennyCount * 0.01m +
            TwoPennyCount * 0.02m +
            FivePennyCount * 0.05m +
            TenPennyCount * 0.1m +
            TwentyPennyCount * 0.2m +
            FiftyPennyCount * 0.5m +
            OnePoundCount +
            FivePoundCount * 5 +
            TenPoundCount * 10 +
            TwentyPoundCount * 20 +
            FiftyPoundCount * 50;

        public Money(int onePennyCount, int twoPennyCount, int fivePennyCount, int tenPennyCount, int twentyPennyCount, int fiftyPennyCount, int onePoundCount, int fivePoundCount, int tenPoundCount, int twentyPoundCount, int fiftyPoundCount): this()
        {
            if (onePennyCount<0 || 
                twoPennyCount<0 ||
                fivePennyCount <0 ||
                tenPennyCount <0 ||
                twentyPennyCount <0 ||
                fiftyPennyCount <0 ||
                onePoundCount <0 ||
                fivePoundCount <0 ||
                tenPoundCount <0 ||
                twentyPoundCount <0 ||
                fiftyPoundCount <0)
                throw new InvalidOperationException();

            OnePennyCount = onePennyCount;
            TwoPennyCount = twoPennyCount;
            FivePennyCount = fivePennyCount;
            TenPennyCount = tenPennyCount;
            TwentyPennyCount = twentyPennyCount;
            FiftyPennyCount = fiftyPennyCount;
            OnePoundCount = onePoundCount;
            FivePoundCount = fivePoundCount;
            TenPoundCount = tenPoundCount;
            TwentyPoundCount = twentyPoundCount;
            FiftyPoundCount = fiftyPoundCount;
        }

        public Money()
        {
        }

        public static Money operator+ (Money money1, Money money2)
        {
            return new Money(
                money1.OnePennyCount + money2.OnePennyCount,
                money1.TwoPennyCount + money2.TwoPennyCount,
                money1.FivePennyCount + money2.FivePennyCount,
                money1.TenPennyCount + money2.TenPennyCount,
                money1.TwentyPennyCount + money2.TwentyPennyCount,
                money1.FiftyPennyCount + money2.FiftyPennyCount,
                money1.OnePoundCount + money2.OnePoundCount,
                money1.FivePoundCount + money2.FivePoundCount,
                money1.TenPoundCount + money2.TenPoundCount,
                money1.TwentyPoundCount + money2.TwentyPoundCount,
                money1.FiftyPoundCount + money2.FiftyPoundCount);

        }

        public static Money operator *(Money money1, int multiplier)
        {
            return new Money(
                money1.OnePennyCount * multiplier,
                money1.TwoPennyCount * multiplier,
                money1.FivePennyCount * multiplier,
                money1.TenPennyCount * multiplier,
                money1.TwentyPennyCount * multiplier,
                money1.FiftyPennyCount * multiplier,
                money1.OnePoundCount * multiplier,
                money1.FivePoundCount * multiplier,
                money1.TenPoundCount * multiplier,
                money1.TwentyPoundCount * multiplier,
                money1.FiftyPoundCount * multiplier);

        }

        public static Money operator -(Money money1, Money money2)
        {
            return new Money(
                money1.OnePennyCount - money2.OnePennyCount,
                money1.TwoPennyCount - money2.TwoPennyCount,
                money1.FivePennyCount - money2.FivePennyCount,
                money1.TenPennyCount - money2.TenPennyCount,
                money1.TwentyPennyCount - money2.TwentyPennyCount,
                money1.FiftyPennyCount - money2.FiftyPennyCount,
                money1.OnePoundCount - money2.OnePoundCount,
                money1.FivePoundCount - money2.FivePoundCount,
                money1.TenPoundCount - money2.TenPoundCount,
                money1.TwentyPoundCount - money2.TwentyPoundCount,
                money1.FiftyPoundCount - money2.FiftyPoundCount);

        }

        protected override bool EqualsCore(Money other) => OnePennyCount == other.OnePennyCount &&
                   TwoPennyCount == other.TwoPennyCount &&
                   FivePennyCount == other.FivePennyCount &&
                   TenPennyCount == other.TenPennyCount &&
                   TwentyPennyCount == other.TwentyPennyCount &&
                   FiftyPennyCount == other.FiftyPennyCount &&
                   OnePoundCount == other.OnePoundCount &&
                   FivePoundCount == other.FivePoundCount &&
                   TenPoundCount == other.TenPoundCount &&
                   TwentyPoundCount == other.TwentyPoundCount &&
                   FiftyPoundCount == other.FiftyPoundCount;

        protected override int GetHashCodeCore()
        {
            unchecked
            {
                var hashCode = OnePennyCount;
                hashCode = (hashCode * 397) ^ TwoPennyCount;
                hashCode = (hashCode * 397) ^ FivePennyCount;
                hashCode = (hashCode * 397) ^ TenPennyCount;
                hashCode = (hashCode * 397) ^ TwentyPennyCount;
                hashCode = (hashCode * 397) ^ FiftyPennyCount;
                hashCode = (hashCode * 397) ^ OnePoundCount;
                hashCode = (hashCode * 397) ^ FivePoundCount;
                hashCode = (hashCode * 397) ^ TenPoundCount;
                hashCode = (hashCode * 397) ^ TwentyPoundCount;
                hashCode = (hashCode * 397) ^ FiftyPoundCount;

                return hashCode;
            }
        }

        public override string ToString()
        {
            if (Amount < 1)
                return "¢" + (Amount * 100).ToString("0");

            return "£" + Amount.ToString("0.00");
        }
    }
}
